package com.nagarro.account.constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AppTestConstants {
public static final	DateFormat INPUT_DATE_FORMAT = new SimpleDateFormat(ApplicationConstants.INPUT_DATETIME_FORMAT, Locale.ENGLISH);	
public static final String FROM_DATE="01.01.2019";
public static final String TO_DATE="01.01.2020";
public static final Double MIN_AMOUNT=Double.valueOf(300);
public static final Double MAX_AMOUNT=Double.valueOf(600);
public static final String ACCOUNT_NUMBER = "0012250016001";
}
