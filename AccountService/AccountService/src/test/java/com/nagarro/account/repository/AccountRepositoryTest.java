/**
 * 
 */
package com.nagarro.account.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nagarro.account.AbstractTestClass;
import com.nagarro.account.entity.Account;

import static org.mockito.Mockito.*;

import java.util.List;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class AccountRepositoryTest extends AbstractTestClass{

	@Autowired
    private AccountRepository accountRepository;

	private static List<Account> mockAccounts;
	
	@SuppressWarnings("unchecked")
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		mockAccounts = (List<Account>) getJsonObject("Account.json",new TypeReference<List<Account>>(){});
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterAll
	static void tearDownAfterClass() throws Exception {
		mockAccounts=null;
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		accountRepository.saveAll(mockAccounts);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterEach
	void tearDown() throws Exception {
		accountRepository.deleteAll();
	}

	/**
	 * Test method for {@link com.nagarro.account.repository.AccountRepository#findByAccountNumber(java.lang.String)}.
	 */
	@DisplayName("JUnit test for findByAccountNumber success case")
	@Test
	void testFindByAccountNumber() {
		String accountNumber= mockAccounts.get(0).getAccountNumber();
		List<Account> accountList = accountRepository.findByAccountNumber(accountNumber);
		assertThat(accountList.size()).isGreaterThanOrEqualTo(1);
	}

	@DisplayName("JUnit test for findByAccountNumber nof found case")
	@Test
	void testFindByAccountNumberNotFound() {
		String accountNumber= "12345678";
		List<Account> accountList = accountRepository.findByAccountNumber(accountNumber);
		assertThat(accountList).isNullOrEmpty();
	}
}
