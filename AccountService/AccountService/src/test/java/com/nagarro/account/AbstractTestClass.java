package com.nagarro.account;

import java.io.File;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class AbstractTestClass {

	protected final static ObjectMapper mapper = new ObjectMapper();

	@SuppressWarnings("unchecked")
	protected static Object getJsonObject(String fileName, Class entityClass) {
		try {
			File file = new File(AbstractTestClass.class.getResource("/TestJsonFiles/" + fileName).getFile());
			return mapper.readValue(file, entityClass);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@SuppressWarnings("unchecked")
	protected static Object getJsonObject(String fileName, TypeReference<?> typeReference) {
		try {
			File file = new File(AbstractTestClass.class.getResource("/TestJsonFiles/" + fileName).getFile());
			return mapper.readValue(file, typeReference);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	protected static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
