package com.nagarro.account.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nagarro.account.AbstractTestClass;
import com.nagarro.account.constants.AppTestConstants;
import com.nagarro.account.entity.Account;
import com.nagarro.account.exception.AccountNotFoundException;
import com.nagarro.account.model.Response;
import com.nagarro.account.service.AccountService;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@RunWith(SpringRunner.class)
@ExtendWith(SpringExtension.class)
@WebMvcTest({ AccountController.class })
public class AccountControllerTest extends AbstractTestClass {

	@Autowired
	private MockMvc mockMvc;

//	    @MockBean
//	    JwtTokenProviderService jwtTokenProviderService;

	@MockBean
	private AccountService<Object> accountService;

	private static List<Account> mockAccounts;

	@SuppressWarnings("unchecked")
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		mockAccounts = (List<Account>) getJsonObject("Account.json", new TypeReference<List<Account>>() {
		});
	}

	@Test
	@WithMockUser("admin") 
	void testGetAllStatement_success() throws Exception {
		Response<Object> response = getResponse(200,mockAccounts, "Total recods  found " + mockAccounts.size());
		 when(accountService.getAllStatements()).thenReturn(response);
		 mockMvc.perform(
	                MockMvcRequestBuilders.get("/api/v1/account")
	                        .contentType(MediaType.APPLICATION_JSON)
	                        .accept(MediaType.APPLICATION_JSON))
	                .andExpect(status().isOk())
	                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
	                .andExpect(jsonPath("$.status", org.hamcrest.Matchers.is(200)))
		            .andExpect(jsonPath("$.data[0].accountId", org.hamcrest.Matchers.is(1)));
	}

	@Test
	@WithMockUser("admin") 
	void testGetStatement_success()  throws Exception{
		Account account=mockAccounts.get(0);
		ArrayList<Account> accounts=new ArrayList<>();
		accounts.add(account);
		Response<Object> response = getResponse(200,accounts, "Recods  found ");
		 when(accountService.getStatementsByDateRange(AppTestConstants.ACCOUNT_NUMBER,null,null,null,null)).thenReturn(response);
		 mockMvc.perform(
	                MockMvcRequestBuilders.get("/api/v1/account/"+AppTestConstants.ACCOUNT_NUMBER)
	                        .contentType(MediaType.APPLICATION_JSON)
	                        .accept(MediaType.APPLICATION_JSON))
	                .andExpect(status().isOk())
	                .andExpect(jsonPath("$.status", org.hamcrest.Matchers.is(200)))
		            .andExpect(jsonPath("$.data[0].accountId", org.hamcrest.Matchers.is(1)));
	}
	
	@Test
	@WithMockUser("user") 
	void testGetStatement_FromDate_success()  throws Exception{
		Account account=mockAccounts.get(0);
		ArrayList<Account> accounts=new ArrayList<>();
		accounts.add(account);
		Response<Object> response = getResponse(200,accounts, "Recods  found ");
		Date fromDate = AppTestConstants.INPUT_DATE_FORMAT.parse(AppTestConstants.FROM_DATE);
		 when(accountService.getStatementsByDateRange(AppTestConstants.ACCOUNT_NUMBER,fromDate,null,null,null)).thenReturn(response);
		 mockMvc.perform(
	                MockMvcRequestBuilders.get("/api/v1/account/"+AppTestConstants.ACCOUNT_NUMBER+"?fromDate="+AppTestConstants.FROM_DATE)
	                        .contentType(MediaType.APPLICATION_JSON)
	                        .accept(MediaType.APPLICATION_JSON))
	                .andExpect(status().isOk())
	                .andExpect(jsonPath("$.status", org.hamcrest.Matchers.is(200)))
		            .andExpect(jsonPath("$.data[0].accountId", org.hamcrest.Matchers.is(1)));
	}
	
	@Test
	@WithMockUser("user") 
	void testGetStatement_FromDate_recordNotfound()  throws Exception{
		Account account=mockAccounts.get(0);
		ArrayList<Account> accounts=new ArrayList<>();
		accounts.add(account);
		Date fromDate = AppTestConstants.INPUT_DATE_FORMAT.parse(AppTestConstants.FROM_DATE);
		 when(accountService.getStatementsByDateRange(AppTestConstants.ACCOUNT_NUMBER,fromDate,null,null,null)).thenThrow(new AccountNotFoundException("Statement not found"));
		 mockMvc.perform(
	                MockMvcRequestBuilders.get("/api/v1/account/"+AppTestConstants.ACCOUNT_NUMBER+"?fromDate="+AppTestConstants.FROM_DATE)
	                        .contentType(MediaType.APPLICATION_JSON)
	                        .accept(MediaType.APPLICATION_JSON))
	                .andExpect(status().isBadRequest())
	                .andExpect(jsonPath("$.status", org.hamcrest.Matchers.is(400)))
		            .andExpect(jsonPath("$.data", org.hamcrest.Matchers.nullValue()));
	}
	@Test
	@WithMockUser("user") 
	void testGetStatement_NotFoundException()  throws Exception{
		Account account=mockAccounts.get(0);
		ArrayList<Account> accounts=new ArrayList<>();
		accounts.add(account);
		 when(accountService.getStatementsByDateRange(AppTestConstants.ACCOUNT_NUMBER,null,null,null,null)).thenThrow(new AccountNotFoundException("Account not found"));
		 mockMvc.perform(
	                MockMvcRequestBuilders.get("/api/v1/account/"+AppTestConstants.ACCOUNT_NUMBER)
	                        .contentType(MediaType.APPLICATION_JSON)
	                        .accept(MediaType.APPLICATION_JSON))
	                .andExpect(status().isBadRequest())
	                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
	                .andExpect(jsonPath("$.status", org.hamcrest.Matchers.is(400)))
		            .andExpect(jsonPath("$.data", org.hamcrest.Matchers.nullValue()));
	}

	@Test
	@WithAnonymousUser
	void testGetStatement_Unauthorized() throws Exception {
		Response<Object> response = getResponse(200,mockAccounts, "Total recods  found " + mockAccounts.size());
		 when(accountService.getAllStatements()).thenReturn(response);
		 mockMvc.perform(
	                MockMvcRequestBuilders.get("/api/v1/account")
	                        .contentType(MediaType.APPLICATION_JSON)
	                        .accept(MediaType.APPLICATION_JSON))
	                .andExpect(status().isUnauthorized());
	      
	}
	private Response<Object> getResponse(int status, Object data, String message){
	return	Response.builder().status(status).data(data).message(message).build();
	}
}
