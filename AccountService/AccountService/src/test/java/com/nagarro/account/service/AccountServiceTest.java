package com.nagarro.account.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nagarro.account.AbstractTestClass;
import com.nagarro.account.constants.AppTestConstants;
import com.nagarro.account.entity.Account;
import com.nagarro.account.entity.Statement;
import com.nagarro.account.model.Response;
import com.nagarro.account.repository.AccountRepository;
import com.nagarro.account.repository.StatementRepository;
import com.nagarro.account.service.impl.AccountServiceImpl;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest extends AbstractTestClass{

	 @Mock
    private AccountRepository accountRepository;

	 @Mock
	 private StatementRepository statementRepository;
	 
	 private AccountService<Object> accountService;
	 
	private static List<Account> mockAccounts;
	
	@SuppressWarnings("unchecked")
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		mockAccounts = (List<Account>) getJsonObject("Account.json",new TypeReference<List<Account>>(){});
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		accountService=new AccountServiceImpl(accountRepository,statementRepository);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@SuppressWarnings("unchecked")
	@DisplayName("JUnit test for testGetAllStatements success case")
	@Test
	void testGetAllStatements() {
		when(accountRepository.findAll()).thenReturn(mockAccounts);
		Response<Object> response = accountService.getAllStatements();
		 assertThat(response.getStatus()).isEqualTo(200);
		 assertThat(response.getData()).isNotNull();
		 List<Account> acountList= (List<Account>) response.getData();
		 assertThat(acountList.size()).isGreaterThanOrEqualTo(2);
	}

	@ParameterizedTest
	@MethodSource("provideParameters")
	void testGetStatementsByDateRange(String accountNumer, String fromDateString, String toDateString, Double minAmount, Double maxAmount ) throws Exception {
		Account account=mockAccounts.get(0);
		List<Account> accountList=new ArrayList<>();
		List<Statement> statementList=account.getStatements();
		accountList.add(account);
		Date fromDate=null;
		if(fromDateString!=null)
		fromDate = AppTestConstants.INPUT_DATE_FORMAT.parse(fromDateString);
		
		Date toDate=null;
		if(toDateString!=null)
		toDate = AppTestConstants.INPUT_DATE_FORMAT.parse(toDateString);
		
		when(accountRepository.findByAccountNumber(anyString())).thenReturn(accountList);
		executeStatementMethod(statementList, account.getAccountId(), fromDate, toDate, minAmount, maxAmount );		
		Response<Object> response = accountService.getStatementsByDateRange(account.getAccountNumber(), fromDate, toDate, minAmount, maxAmount );
		 assertThat(response.getStatus()).isEqualTo(200);
		 assertThat(response.getData()).isNotNull();
		 Account acount= (Account) response.getData();
		 assertThat(account.getStatements()).isNotEmpty();
	}

	private static Stream<Arguments> provideParameters() {
	    return Stream.of(
	            Arguments.of(AppTestConstants.ACCOUNT_NUMBER, AppTestConstants.FROM_DATE, null,null,null),
	            Arguments.of(AppTestConstants.ACCOUNT_NUMBER, null, AppTestConstants.TO_DATE,null,null),
	            Arguments.of(AppTestConstants.ACCOUNT_NUMBER, AppTestConstants.FROM_DATE, AppTestConstants.TO_DATE,null,null),
	            Arguments.of(AppTestConstants.ACCOUNT_NUMBER, null, null,AppTestConstants.MIN_AMOUNT,null),
	            Arguments.of(AppTestConstants.ACCOUNT_NUMBER, null, null,null,AppTestConstants.MAX_AMOUNT),
	            Arguments.of(AppTestConstants.ACCOUNT_NUMBER, null, null,AppTestConstants.MIN_AMOUNT,AppTestConstants.MAX_AMOUNT)
	    );
	}
	
	private void executeStatementMethod(List<Statement> statementList, Long accountId, Date fromDate, Date toDate, Double minAmount, Double maxAmount) {
		if(fromDate!=null && toDate==null && minAmount==null && maxAmount==null)
		{
		when(statementRepository.findByAccountIdFromDate(accountId,fromDate)).thenReturn(statementList);
		}
		else if(fromDate==null && toDate!=null && minAmount==null && maxAmount==null)
		{
		when(statementRepository.findByAccountIdToDate(accountId,toDate)).thenReturn(statementList);
		}
		else if(fromDate!=null && toDate!=null && minAmount==null && maxAmount==null)
		{
		when(statementRepository.findByAccountIdBetweenDate(accountId,fromDate,toDate)).thenReturn(statementList);
		}
		else if(fromDate==null && toDate==null && minAmount!=null && maxAmount==null)
		{
		when(statementRepository.findByAccountIdMinAmount(accountId,minAmount)).thenReturn(statementList);
		}
		else if(fromDate==null && toDate==null && minAmount==null && maxAmount!=null)
		{
		when(statementRepository.findByAccountIdMaxAmount(accountId,maxAmount)).thenReturn(statementList);
		}
		else if(fromDate==null && toDate==null && minAmount !=null && maxAmount != null)
		{
		when(statementRepository.findByAccountIdBetweenAmount(accountId,minAmount,maxAmount)).thenReturn(statementList);
		}
	}
}
