package com.nagarro.account.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.nagarro.account.AbstractTestClass;
import com.nagarro.account.constants.AppTestConstants;
import com.nagarro.account.entity.Account;
import com.nagarro.account.entity.Statement;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
public class StatementRepositoryTest extends AbstractTestClass{

	@Autowired
	private StatementRepository statementRepository;
	
	private static Account mockAccount;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		mockAccount=( (List<Account>) getJsonObject("Account.json",new TypeReference<List<Account>>(){})).get(0);
		
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		mockAccount=null;
	}

	@BeforeEach
	void setUp() throws Exception {
		statementRepository.saveAll(mockAccount.getStatements());
	}

	@AfterEach
	void tearDown() throws Exception {
		statementRepository.deleteAll();
	}

	@DisplayName("JUnit test for findByAccountNumber success case")
	@Test
	void testFindByAccountId() {
		List<Statement> statements = statementRepository.findByAccountId(mockAccount.getAccountId());
		assertThat(statements.size()).isGreaterThanOrEqualTo(8);
	}

	@DisplayName("JUnit test for testFindByAccountIdFromDate success case")
	@Test
	void testFindByAccountIdFromDate() throws Exception {
		Date fromDate = AppTestConstants.INPUT_DATE_FORMAT.parse(AppTestConstants.FROM_DATE);
		List<Statement> statements = statementRepository.findByAccountIdFromDate(mockAccount.getAccountId(),fromDate);
		assertThat(statements.size()).isGreaterThanOrEqualTo(8);
	}

	@DisplayName("JUnit test for testFindByAccountIdToDate success case")
	@Test
	void testFindByAccountIdToDate() throws Exception {
		Date toDate = AppTestConstants.INPUT_DATE_FORMAT.parse(AppTestConstants.TO_DATE);
		List<Statement> statements = statementRepository.findByAccountIdToDate(mockAccount.getAccountId(),toDate);
		assertThat(statements.size()).isGreaterThanOrEqualTo(4);
	}

	@DisplayName("JUnit test for testFindByAccountIdBetweenDate success case")
	@Test
	void testFindByAccountIdBetweenDate() throws Exception{
		Date fromDate = AppTestConstants.INPUT_DATE_FORMAT.parse(AppTestConstants.FROM_DATE);
		Date toDate = AppTestConstants.INPUT_DATE_FORMAT.parse(AppTestConstants.TO_DATE);
		List<Statement> statements = statementRepository.findByAccountIdBetweenDate(mockAccount.getAccountId(),fromDate,toDate);
		assertThat(statements.size()).isGreaterThanOrEqualTo(4);
	}

	@DisplayName("JUnit test for testFindByAccountIdMinAmount success case")
	@Test
	void testFindByAccountIdMinAmount() {
		List<Statement> statements = statementRepository.findByAccountIdMinAmount(mockAccount.getAccountId(),AppTestConstants.MIN_AMOUNT);
		assertThat(statements.size()).isGreaterThanOrEqualTo(4);
	}

	@DisplayName("JUnit test for testFindByAccountIdMaxAmount success case")
	@Test
	void testFindByAccountIdMaxAmount() {
		List<Statement> statements = statementRepository.findByAccountIdMaxAmount(mockAccount.getAccountId(),AppTestConstants.MAX_AMOUNT);
		assertThat(statements.size()).isGreaterThanOrEqualTo(4);
	}

	@DisplayName("JUnit test for testFindByAccountIdBetweenAmount success case")
	@Test
	void testFindByAccountIdBetweenAmount() {
		List<Statement> statements = statementRepository.findByAccountIdBetweenAmount(mockAccount.getAccountId(),AppTestConstants.MIN_AMOUNT,AppTestConstants.MAX_AMOUNT);
		assertThat(statements.size()).isGreaterThanOrEqualTo(3);
	}

}
