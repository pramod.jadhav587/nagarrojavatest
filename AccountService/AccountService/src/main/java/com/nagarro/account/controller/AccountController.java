package com.nagarro.account.controller;

import com.nagarro.account.constants.ApplicationConstants;
import com.nagarro.account.exception.AccountNotFoundException;
import com.nagarro.account.model.Response;
import com.nagarro.account.service.AccountService;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping("/api/v1/account")
@Validated
public class AccountController {
	
	private static final Logger LOGGER = LogManager.getLogger(AccountController.class);
	private AccountService<Object> accountService;
	
	public AccountController(AccountService<Object> accountService) {
		this.accountService=accountService;
	}
	
	@Secured("ROLE_ADMIN")
	@GetMapping
    public Response<Object> getAllStatement(){
		LOGGER.info(" Start: getAllStatement");
       return accountService.getAllStatements();
	}

	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	  @GetMapping(value = "/{accountNumer}") 
	  public Response<Object> getStatement(@PathVariable("accountNumer") @Valid String accountNumer,
	  @RequestParam(name = "fromDate", required = false)  @DateTimeFormat(pattern=ApplicationConstants.INPUT_DATETIME_FORMAT) @Valid Date  fromDate,
	  @RequestParam(name = "toDate", required = false) @DateTimeFormat(pattern=ApplicationConstants.INPUT_DATETIME_FORMAT) @Valid Date toDate,
	  @RequestParam(name = "minAmount", required = false) @Valid Double minAmount,
	  @RequestParam(name = "maxAmount", required = false) @Valid Double maxAmount){
		  LOGGER.info("Start:  getStatement ");
	   return accountService.getStatementsByDateRange(accountNumer, fromDate, toDate,minAmount,maxAmount);
	 
	  }
	

}
