package com.nagarro.account.repository;


import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nagarro.account.entity.Account;
import com.nagarro.account.entity.Statement;

@Repository
public interface StatementRepository extends JpaRepository<Statement, Long>{
 List<Statement> findByAccountId(Long accountId);
 
 @Query(value = "from Statement s where s.accountId = :accountId AND TO_DATE(s.datefield ,'DD.MM.YYYY') >= :fromDate")
 public List<Statement> findByAccountIdFromDate(@Param("accountId") Long accountId, @Param("fromDate") Date fromDate);

 @Query(value = "from Statement s where s.accountId = :accountId AND TO_DATE(s.datefield ,'DD.MM.YYYY') <= :toDate")
 public List<Statement> findByAccountIdToDate(@Param("accountId") Long accountId, @Param("toDate") Date toDate);

 @Query(value = "from Statement s where s.accountId = :accountId AND TO_DATE(s.datefield ,'DD.MM.YYYY') BETWEEN  :fromDate AND :toDate")
 public List<Statement> findByAccountIdBetweenDate(@Param("accountId") Long accountId,@Param("fromDate") Date fromDate, @Param("toDate") Date toDate);

 @Query(value = "from Statement s where s.accountId = :accountId AND TO_NUMBER(amount)  >=  :minAmount")
 public List<Statement> findByAccountIdMinAmount(@Param("accountId") Long accountId,@Param("minAmount") Double minAmount);

 @Query(value = "from Statement s where s.accountId = :accountId AND TO_NUMBER(amount)  <=  :maxAmount")
 public List<Statement> findByAccountIdMaxAmount(@Param("accountId") Long accountId,@Param("maxAmount") Double maxAmount);

 @Query(value = "from Statement s where s.accountId = :accountId AND TO_NUMBER(amount)  BETWEEN  :minAmount AND :maxAmount")
 public List<Statement> findByAccountIdBetweenAmount(@Param("accountId") Long accountId,@Param("minAmount") Double minAmount,@Param("maxAmount") Double maxAmount);

}
