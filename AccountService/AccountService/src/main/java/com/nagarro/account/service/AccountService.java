package com.nagarro.account.service;

import java.util.Date;

import com.nagarro.account.exception.ApplicationException;
import com.nagarro.account.model.Response;

public interface AccountService<E> {

	public Response<E> getAllStatements()throws ApplicationException; 
	public Response<E> getStatementsByDateRange(String accountNumer, Date fromDate, Date toDate, Double minAmount, Double maxAmount)throws ApplicationException; 
}
