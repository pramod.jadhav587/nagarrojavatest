package com.nagarro.account.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nagarro.account.constants.ApplicationConstants;

public class AppJacksonBuilderConfig {

	   @Bean
	    @Primary
	    public Jackson2ObjectMapperBuilder jackson2ObjectMapperBuilder() {
	        return new Jackson2ObjectMapperBuilder()
	                .serializers(ApplicationConstants.LOCAL_DATETIME_SERIALIZER)
	                .serializationInclusion(JsonInclude.Include.NON_NULL);
	    }
}
