package com.nagarro.account.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.nagarro.account.util.ApplicationUtil;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Entity
@Table(name = "account")
public class Account implements Serializable, Cloneable{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private long accountId;
	
	@Column(name = "account_type")
	private String accountType;

	@Column(name = "account_number")
	private String accountNumber;
	
//	@OneToMany(mappedBy = "account", fetch = FetchType.LAZY,
//            cascade = CascadeType.ALL)
	@Transient
    private List<Statement> statements;

	@Override
	public Object clone() throws CloneNotSupportedException {
		Account account=new Account();
		account.setAccountId(this.getAccountId());
		account.setAccountNumber(ApplicationUtil.encodeString("#", this.getAccountNumber().length()));
		account.setAccountType(this.getAccountType());
		account.setStatements(this.getStatements());
		return account;
	}
	
	
}
