package com.nagarro.account.exception;

import com.nagarro.account.constants.ApplicationConstants;
import com.nagarro.account.model.Error;
import com.nagarro.account.model.Response;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class AppExceptionController extends ResponseEntityExceptionHandler{

	  @ExceptionHandler(value = {Exception.class, java.lang.UnsupportedOperationException.class})
	  public final Response<Object> handleAllExceptions(Exception ex, WebRequest request) {
		   Error error= Error.builder()
	                .errorCode(1005)
	                .message(ex.getMessage())
	                .description(request.getDescription(false))
	                .build();
	       List<Error> errorList=new ArrayList<>(1);
	        errorList.add(error);
	        String errorMessage =null;
	        if(ex.getLocalizedMessage() != null && ex.getLocalizedMessage().contains("java.lang.IllegalArgumentException"))
	        	errorMessage = "Incorrect input values";
	       Response<Object> response=Response.builder()
	               .status(HttpStatus.BAD_REQUEST.value())
	               .message(errorMessage)
	               .errors(errorList)
	               .build();
        return response;
	  }
	 
	  @ExceptionHandler(value = {org.hibernate.exception.DataException.class})
	  public final Response<Object> handleDataException(Exception ex, WebRequest request) {
		   Error error= Error.builder()
	                .errorCode(1005)
	                .message(ex.getMessage())
	                .description(request.getDescription(false))
	                .build();
	       List<Error> errorList=new ArrayList<>(1);
	        errorList.add(error);
	        String errorMessage = "Internal server error";
	       Response<Object> response=Response.builder()
	               .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
	               .message(errorMessage)
	               .errors(errorList)
	               .build();
        return response;
	  }
	  
    @ExceptionHandler(value = {AccountNotFoundException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public final Response<Object> accountNotFoundException(AccountNotFoundException ex, WebRequest request) {
        Error error= Error.builder()
                .errorCode(1001)
                .message(ex.getMessage())
                .description(request.getDescription(false))
                .build();
        List<Error> errorList=new ArrayList<>(1);
        errorList.add(error);
       Response<Object> response=Response.builder()
               .status(HttpStatus.BAD_REQUEST.value())
               .message(ApplicationConstants.RECORD_NOTFOUND_ERROR_MSG)
               .errors(errorList)
               .build();

        return response;
    }

    @ExceptionHandler(value = {ApplicationException.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Response<Object> applicationException(ApplicationException ex, WebRequest request) {
   Error error= Error.builder()
                .errorCode(1500)
                .message(ex.getMessage())
                .description(request.getDescription(false))
                .build();
        List<Error> errorList=new ArrayList<>(1);
        errorList.add(error);
        Response<Object> response=Response.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(HttpStatus.INTERNAL_SERVER_ERROR.name())
                .errors(errorList)
                .build();
        return response;
    }
    
    @ExceptionHandler(value = {CloneNotSupportedException.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Response<Object> cloneNotSupportedException(ApplicationException ex, WebRequest request) {
   Error error= Error.builder()
                .errorCode(1600)
                .message(ex.getMessage())
                .description(request.getDescription(false))
                .build();
        List<Error> errorList=new ArrayList<>(1);
        errorList.add(error);
        Response<Object> response=Response.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(HttpStatus.INTERNAL_SERVER_ERROR.name())
                .errors(errorList)
                .build();
        return response;
    }
    
    @ExceptionHandler({ ConstraintViolationException.class })
    public ResponseEntity<Object> handleConstraintViolation(
      ConstraintViolationException ex, WebRequest request) {
    	 List<Error> errorList=new ArrayList<>(1);
        for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
        	  Error error= Error.builder()
                      .errorCode(1200)
                      .message(violation.getRootBeanClass().getName() + " " + 
                              violation.getPropertyPath() + ": " + violation.getMessage())
                      .description(request.getDescription(false))
                      .build();
              errorList.add(error);
         }
      Response<Object> response=Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message(HttpStatus.BAD_REQUEST.name())
                .errors(errorList)
                .build();
        return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
    }
    
    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
      MethodArgumentTypeMismatchException ex, WebRequest request) {
    	 List<Error> errorList=new ArrayList<>(1);
        String errorMessage = 
          ex.getName() + " should be of type " + ex.getRequiredType().getName();

        Error error= Error.builder()
                .errorCode(1400)
                .message(errorMessage)
                .description(request.getDescription(false))
                .build();
        errorList.add(error);
        Response<Object> response=Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message(HttpStatus.BAD_REQUEST.name())
                .errors(errorList)
                .build();
        return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
    }
    
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
    	 List<Error> errorList=new ArrayList<>(1);
      for(ObjectError objError : ex.getBindingResult().getAllErrors()) {
    	  Error error= Error.builder()
                  .errorCode(1200)
                  .message(objError.getDefaultMessage())
                  .description(request.getDescription(false))
                  .build();
          errorList.add(error);
      }
      Response<Object> response=Response.builder()
              .status(HttpStatus.BAD_REQUEST.value())
              .message(HttpStatus.BAD_REQUEST.name())
              .errors(errorList)
              .build();
      return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
    }
    
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
      MissingServletRequestParameterException ex, HttpHeaders headers, 
      HttpStatus status, WebRequest request) {
    	 List<Error> errorList=new ArrayList<>(1);
       	  Error error= Error.builder()
                    .errorCode(1300)
                    .message(ex.getLocalizedMessage())
                    .description(request.getDescription(false))
                    .build();
            errorList.add(error);
        
        Response<Object> response=Response.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .message(HttpStatus.BAD_REQUEST.name())
                .errors(errorList)
                .build();
        return new ResponseEntity<Object>(response, HttpStatus.BAD_REQUEST);
    }
    
    
}
