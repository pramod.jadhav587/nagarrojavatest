package com.nagarro.account.model;

import lombok.Builder;
import org.springframework.data.annotation.Immutable;

@Immutable
@Builder
public class Error {
    private String message;
    private String description;
    private int errorCode;
	public String getMessage() {
		return message;
	}
	public String getDescription() {
		return description;
	}
	public int getErrorCode() {
		return errorCode;
	}
    
    
}
