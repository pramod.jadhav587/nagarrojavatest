package com.nagarro.account.constants;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;

public class ApplicationConstants {
	    public static final String DATETIME_FORMAT = "dd-MM-yyyy HH:mm";
	    public static final LocalDateTime FIXED_DATE = LocalDateTime.now();
	    public static LocalDateTimeSerializer LOCAL_DATETIME_SERIALIZER = new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(DATETIME_FORMAT));
	    public static final String INPUT_DATETIME_FORMAT = "dd.MM.yyyy";
	    public static final String RECORD_NOTFOUND_ERROR_MSG="Record not found error.";
}
