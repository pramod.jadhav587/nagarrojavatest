package com.nagarro.account.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.nagarro.account.entity.Account;
import com.nagarro.account.entity.Statement;
import com.nagarro.account.exception.AccountNotFoundException;
import com.nagarro.account.exception.ApplicationException;
import com.nagarro.account.model.Response;
import com.nagarro.account.repository.AccountRepository;
import com.nagarro.account.repository.StatementRepository;
import com.nagarro.account.service.AccountService;
import com.nagarro.account.util.ApplicationUtil;

@Service
@Transactional
public class AccountServiceImpl implements AccountService<Object> {

	 private static final Logger LOGGER = LogManager.getLogger(AccountServiceImpl.class);
	 
	private AccountRepository accountRepository;

	private StatementRepository statementRepository;

	public AccountServiceImpl(AccountRepository accountRepository, StatementRepository statementRepository) {
		this.accountRepository = accountRepository;
		this.statementRepository = statementRepository;
	}

	@Override
	public Response<Object> getAllStatements() throws ApplicationException {
		List<Account> accountList = accountRepository.findAll();
		List<Account> encodeAccountList=new ArrayList<>();
		if (ApplicationUtil.isNullOREmpty(accountList))
			throw new AccountNotFoundException(" Record not found. ");
		for (Account a : accountList) {
			List<Statement> statements = statementRepository.findByAccountId(a.getAccountId());
			a.setStatements(statements);
		}
		Response<Object> response = Response.builder().status(200).data(accountList)
				.message("Total recods  found " + accountList.size()).build();
		return response;
	}

	@Override
	public Response<Object> getStatementsByDateRange(String accountNumer, Date fromDate, Date toDate, Double minAmount,
			Double maxAmount) throws ApplicationException {
		LOGGER.info("Start: getStatementsByDateRange {}",accountNumer);
	List<Account> accountList = accountRepository.findByAccountNumber(accountNumer);
		if (ApplicationUtil.isNullOREmpty(accountList))
			throw new AccountNotFoundException("For " + accountNumer + " record not found ");
		for (Account a : accountList) {
			LOGGER.info(" Fount Account Id "+a.getAccountId());
			List<Statement> statements = getStatementListByDateRange(a.getAccountId(), fromDate, toDate, minAmount,
					maxAmount);
			LOGGER.info(" Fount statements "+statements.size());
			if (ApplicationUtil.isNullOREmpty(statements))
				throw new AccountNotFoundException("For " + accountNumer + " statements not found ");
			a.setStatements(statements);
		}
		Account account = accountList.get(0);
		Account encodeAccount = null;
		try {
			encodeAccount = (Account) account.clone();
			return Response.builder().status(200).data(encodeAccount).message("Recod is found").build();
		} catch (CloneNotSupportedException e) {
			throw new AccountNotFoundException("For " +account.getAccountId()  + " encode error ");
		}
	}

	private List<Statement> getStatementListByDateRange(Long accountId, Date fromDate, Date toDate, Double minAmount,
			Double maxAmount) {
		if (accountId != null && fromDate == null && toDate == null && minAmount == null && maxAmount == null) {
			fromDate = new Date();
			toDate = DateUtils.addMonths(fromDate, 3);
			return statementRepository.findByAccountIdBetweenDate(accountId, fromDate, toDate);
		} else if (accountId != null && fromDate != null && toDate == null && minAmount == null && maxAmount == null) {
			return statementRepository.findByAccountIdFromDate(accountId, fromDate);
		} else if (accountId != null && fromDate == null && toDate != null && minAmount == null && maxAmount == null) {
			return statementRepository.findByAccountIdToDate(accountId, toDate);
		} else if (accountId != null && fromDate != null && toDate != null && minAmount == null && maxAmount == null) {
			return statementRepository.findByAccountIdBetweenDate(accountId, fromDate, toDate);
		} else if (accountId != null && fromDate == null && toDate == null && minAmount != null && maxAmount == null) {
			return statementRepository.findByAccountIdMinAmount(accountId, minAmount);
		} else if (accountId != null && fromDate == null && toDate == null && minAmount == null && maxAmount != null) {
			return statementRepository.findByAccountIdMaxAmount(accountId, maxAmount);
		} else if (accountId != null && fromDate == null && toDate == null && minAmount != null && maxAmount != null) {
			return statementRepository.findByAccountIdBetweenAmount(accountId, minAmount, maxAmount);
		}
		return null;
	}

}
