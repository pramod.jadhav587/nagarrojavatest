package com.nagarro.account.config;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig {

	@Autowired
	private AuthenticationEntryPoint authEntryPoint;
	
	@Primary
	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration authenticationConfiguration)
			throws Exception {
		AuthenticationManager authenticationManager=authenticationConfiguration.getAuthenticationManager();
		return authenticationManager;
	}

	@Bean
	public InMemoryUserDetailsManager configAuthentication() {
		List<UserDetails> userDetailsList = new ArrayList<>();
		 
		List<GrantedAuthority> adminAuthority = new ArrayList<>();
	       adminAuthority.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
	       UserDetails admin= new User("admin", "{noop}admin", adminAuthority);
	       userDetailsList.add(admin);

	       List<GrantedAuthority> userAuthority = new ArrayList<>();
	       userAuthority.add(new SimpleGrantedAuthority("ROLE_USER"));
	       UserDetails user= new User("user", "{noop}user", userAuthority);
	       userDetailsList.add(user);
	       
		return new InMemoryUserDetailsManager(userDetailsList);
	}
	
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		 http.csrf()
         .disable()
         .authorizeRequests().antMatchers("/swagger-ui/**","/api-docs/**","/javainuse-openapi/**").permitAll()
          .antMatchers("/api/**").access("hasAnyAuthority('ROLE_ADMIN','ROLE_USER')")
         .and()
         .httpBasic()
         .authenticationEntryPoint(authEntryPoint)
         .and()
         .sessionManagement()
         .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		return http.build();
	}

	@Bean
	public WebSecurityCustomizer webSecurityCustomizer() {
		return (web) -> web.ignoring().antMatchers("/login", "/swagger-ui.html");
	}


}