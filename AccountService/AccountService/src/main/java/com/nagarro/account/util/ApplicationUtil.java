package com.nagarro.account.util;

import java.util.Collection;

import org.apache.commons.lang3.StringUtils;



public final class ApplicationUtil {
	
      public static boolean isNullOREmpty(Object obj) {
    	  if(obj instanceof String)
    		  return obj==null || obj.toString().isEmpty();
    	  if(obj instanceof Collection)
    		  return obj == null || ((Collection<?>)obj).isEmpty();
    	  return false;
      }
      
      public static String encodeString(String str, int length) {
    	  return StringUtils.repeat(str, 10);
      }
}
