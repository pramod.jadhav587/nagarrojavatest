package com.nagarro.account.model;

import lombok.Builder;
import org.springframework.data.annotation.Immutable;

import com.nagarro.account.constants.ApplicationConstants;

import java.time.LocalDateTime;
import java.util.List;

@Immutable
@Builder
public class Response<E> {
    private String message;
    private int status;
    private E data;
    private LocalDateTime timestamp;
    private List<Error> errors;

    public LocalDateTime getTimestamp() {
    	  if(this.timestamp == null)
    		  this.timestamp=ApplicationConstants.FIXED_DATE;
        return this.timestamp;
    }

	public String getMessage() {
		return message;
	}

	public int getStatus() {
		return status;
	}

	public E getData() {
		return data;
	}

	public List<Error> getErrors() {
		return errors;
	}

   
}
