package com.nagarro.account.entity;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Entity
@Table(name = "statement")
public class Statement {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private long statementId;
	
	@Column(name = "datefield")
	private String datefield;

	@Column(name = "amount")
	private String amount;
	
//	 @ManyToOne(fetch = FetchType.LAZY, optional = false)
//@JoinColumn(name = "account_id", nullable = false)
	@Column(name = "account_id")
   private Long accountId;
}
