package com.nagarro.account.config;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

import com.zaxxer.hikari.HikariDataSource;

@Configuration
@EnableJpaRepositories(basePackages = "com.nagarro.account.repository", entityManagerFactoryRef = "accountEntityManagerFactory", transactionManagerRef = "accountTransactionManager")
public class DataSourceConfiguration {

	@Primary
	@Bean
	@ConfigurationProperties("spring.datasource.account")
	public DataSourceProperties accountDataSourceProperties() {
		return new DataSourceProperties();
	}

	@Bean
	@ConfigurationProperties("spring.datasource.account.configuration")
	public DataSource accountDataSource() {
		return accountDataSourceProperties().initializeDataSourceBuilder().type(HikariDataSource.class).build();
	}

	@Bean(name = "accountEntityManagerFactory")
	public LocalContainerEntityManagerFactoryBean accountEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(accountDataSource()).properties(getHibernateProperties()).packages("com.nagarro.account.entity").build();
	}

	@Bean
	public PlatformTransactionManager accountTransactionManager(
			final @Qualifier("accountEntityManagerFactory") LocalContainerEntityManagerFactoryBean accountEntityManagerFactory) {
		return new JpaTransactionManager(accountEntityManagerFactory.getObject());
	}
	
	  private Map<String, Object>  getHibernateProperties() {
	        Map<String, Object> properties = new HashMap<>();
	        properties.put("hibernate.dialect", "org.hibernate.dialect.SQLServerDialect");
	        properties.put("hibernate.hbm2ddl.auto","update");
	        properties.put("hibernate.show-sql","true");
	        return properties;
	    }
}
