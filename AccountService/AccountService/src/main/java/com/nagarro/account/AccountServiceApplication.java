package com.nagarro.account;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication
public class AccountServiceApplication {
	private static final Logger LOGGER = LogManager.getLogger(AccountServiceApplication.class);
	public static void main(String[] args) {
		LOGGER.info("*** Application Start");
		SpringApplication.run(AccountServiceApplication.class, args);
	}

}
